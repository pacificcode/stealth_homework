# stealth_homework

Create a Git repository on gitlab.com that uses GitLab CI/CD to create a binary package for Git itself by downloading the Git source code, building it, and performing a staged install. Use CentOS 7 as the platform, use /usr/local as the prefix, and output a tarball of the binary package as a job artifact. You do not need to build the documentation.
